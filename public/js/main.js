'use strict';

var config = {
apiKey: "AIzaSyBYfU-K_NPNwJ27TadEqZesb7J_DDDlwys",
        authDomain: "webrtcdc.firebaseapp.com",
        databaseURL: "https://webrtcdc.firebaseio.com",
        projectId: "webrtcdc",
        storageBucket: "webrtcdc.appspot.com",
        messagingSenderId: "233688362783"
};

let peerConn;
let dataChannel;
let fileReader;
const bitrateDiv = document.querySelector('div#bitrate');
const fileInput = document.querySelector('input#fileInput');
const abortButton = document.querySelector('button#abortButton');
const downloadAnchor = document.querySelector('a#download');
const progressBar = document.querySelector('progress#progressBar');
const statusMessage = document.querySelector('span#status');
const sendFileButton = document.querySelector('button#sendFile');

let sender = 0;
let receiveBuffer = [];
let receivedSize = 0;

let bytesPrev = 0;
let timestampPrev = 0;
let timestampStart;
let statsInterval = null;
let bitrateMax = 0;
var yourId = Math.floor(Math.random()*1000000000);
var database;
let filename = "";
let filesize = 0;

function init() {
    sender = 0;
    filename = "";
    filesize = 0;
    database = firebase.database().ref();
    database.on('child_added', readMessage);

    if (peerConn) {
        dataChannel.close();
        peerConn.close();
    }

    sendFileButton.addEventListener('click', () => {
            sender = 1;
            createConnection();
            sendOffer();
            });

    fileInput.addEventListener('change', handleFileInputChange, false);
    abortButton.addEventListener('click', () => {
            if (fileReader && fileReader.readyState === 1) {
            console.log('Abort read!');
            fileReader.abort();
            }
            });

    // re-enable the file select
    fileInput.disabled = false;
    abortButton.disabled = true;
    sendFileButton.disabled = false;
}

function sendMessage(senderId, data) {
    var msg = database.push({ sender: senderId, message: data });
    msg.remove();
}

function readMessage(data) {
    var msg = JSON.parse(data.val().message);
    var sender = data.val().sender;
    if (sender != yourId) {
        if (msg.ice != undefined) {
            peerConn.addIceCandidate(new RTCIceCandidate(msg.ice));
        }else if (msg.filename != undefined) {
            filename = msg.filename;
        }else if (msg.filesize != undefined) {
            filesize = msg.filesize;
            progressBar.max = filesize;
        } else if (msg.sdp.type == "offer") {
            createConnection();
            peerConn.setRemoteDescription(new RTCSessionDescription(msg.sdp))
                .then(() => peerConn.createAnswer())
                .then(answer => peerConn.setLocalDescription(answer))
                .then(() => sendMessage(yourId, JSON.stringify({'sdp': peerConn.localDescription})));
        } else if (msg.sdp.type == "answer") {
            peerConn.setRemoteDescription(new RTCSessionDescription(msg.sdp));
        }
    }
}

async function handleFileInputChange() {
    let file = fileInput.files[0];
    if (!file) {
        console.log('No file chosen');
    } else {
        sendFileButton.disabled = false;
    }
}

function createConnection() {
    if (peerConn) {
        console.log("skip createConnection");
        return
    }
    abortButton.disabled = false;
    sendFileButton.disabled = true;
    peerConn = new RTCPeerConnection();
    console.log('Created local peer connection object peerConn');

    if (sender == 1) {
        dataChannel = peerConn.createDataChannel('MyDataChannel');
        dataChannel.binaryType = 'arraybuffer';
        console.log('Created data channel');

        dataChannel.addEventListener('open', onSendChannelStateChange);
        dataChannel.addEventListener('close', onSendChannelStateChange);
        dataChannel.addEventListener('onmessage', onReceiveMessageCallback);
        dataChannel.addEventListener('error', error => console.error('Error in dataChannel:', error));
    }else {
        peerConn.addEventListener('datachannel', receiveChannelCallback);
    }

    receivedSize = 0;
    bitrateMax = 0;
    downloadAnchor.textContent = '';
    downloadAnchor.removeAttribute('download');
    if (downloadAnchor.href) {
        URL.revokeObjectURL(downloadAnchor.href);
        downloadAnchor.removeAttribute('href');
    }

    peerConn.addEventListener('icecandidate', event => {
            console.log('Local ICE candidate: ', event.candidate);
            event.candidate?sendMessage(yourId, JSON.stringify({'ice': event.candidate})):console.log("Sent All Ice");
            });
}

function receiveChannelCallback(event) {
    console.log('Receive Channel Callback');
    dataChannel = event.channel;
    dataChannel.binaryType = 'arraybuffer';
    dataChannel.onmessage = onReceiveMessageCallback;
    dataChannel.onopen = onReceiveChannelStateChange;
    dataChannel.onclose = onReceiveChannelStateChange;

    receivedSize = 0;
    bitrateMax = 0;
    downloadAnchor.textContent = '';
    downloadAnchor.removeAttribute('download');
    if (downloadAnchor.href) {
        URL.revokeObjectURL(downloadAnchor.href);
        downloadAnchor.removeAttribute('href');
    }
}

function sendOffer() {
    peerConn.createOffer()
        .then(offer => peerConn.setLocalDescription(offer))
        .then(() => sendMessage(yourId, JSON.stringify({'sdp': peerConn.localDescription})));
    fileInput.disabled = true;
}

function sendData() {
    const file = fileInput.files[0];
    console.log(`File is ${[file.name, file.size, file.type, file.lastModified].join(' ')}`);

    // Handle 0 size files.
    statusMessage.textContent = '';
    downloadAnchor.textContent = '';
    if (file.size === 0) {
        bitrateDiv.innerHTML = '';
        statusMessage.textContent = 'File is empty, please select a non-empty file';
        closeDataChannels();
        return;
    }
    sendMessage(yourId, JSON.stringify({'filename':file.name}));
    sendMessage(yourId, JSON.stringify({'filesize':file.size}));
    progressBar.max = file.size;
    const chunkSize = 16384;
    fileReader = new FileReader();
    let offset = 0;
    fileReader.addEventListener('error', error => console.error('Error reading file:', error));
    fileReader.addEventListener('abort', event => console.log('File reading aborted:', event));
    fileReader.addEventListener('load', e => {
            console.log('FileRead.onload ', e);
            dataChannel.send(e.target.result);
            offset += e.target.result.byteLength;
            progressBar.value = offset;
            if (offset < file.size) {
            readSlice(offset);
            }else {
            console.log('finished');
            dataChannel.close();
            }
            });
    const readSlice = o => {
        console.log('readSlice ', o);
        const slice = file.slice(offset, o + chunkSize);
        fileReader.readAsArrayBuffer(slice);
    };
    readSlice(0);
}

function closeDataChannels() {
    console.log('Closing data channels');
    dataChannel.close();
    console.log(`Closed data channel with label: ${dataChannel.label}`);
    peerConn.close();
    peerConn = null;
    console.log('Closed peer connections');
}

function onReceiveMessageCallback(event) {
    console.log("onReceiveMessageCallback");
    console.log(`Received Message ${event.data.byteLength}`);
    receiveBuffer.push(event.data);
    receivedSize += event.data.byteLength;

    progressBar.value = receivedSize;
}

function updateStats() {
    const received = new Blob(receiveBuffer);
    receiveBuffer = [];

    downloadAnchor.href = URL.createObjectURL(received);
    downloadAnchor.download = filename;
    downloadAnchor.textContent =
        `Click to download '${filename}'`;
    downloadAnchor.style.display = 'block';

    var bitrate = Math.round(receivedSize * 8 /
            ((new Date()).getTime() - timestampStart));
    bitrate = Math.round(bitrate/1024.0);
    bitrateDiv.innerHTML
        = `<strong>Average Bitrate:</strong> ${bitrate} Mbits/sec (max: ${bitrateMax} kbits/sec)`;

    if (statsInterval) {
        clearInterval(statsInterval);
        statsInterval = null;
    }
    closeDataChannels();
    init();
}

function onSendChannelStateChange() {
    const readyState = dataChannel.readyState;
    console.log(`Data channel state is: ${readyState}`);
    if (sender == 1 && readyState === 'open') {
        sendData();
    }else if (readyState === 'closed') {
        init();
    }
}

async function onReceiveChannelStateChange() {
    const readyState = dataChannel.readyState;
    console.log(`Receive channel state is: ${readyState}`);
    if (readyState === 'open') {
        timestampStart = (new Date()).getTime();
        timestampPrev = timestampStart;
        statsInterval = setInterval(showStats, 500);
        await showStats();
    }else {
        updateStats();
    }
}

// display bitrate statistics.
async function showStats() {
    if (peerConn.iceConnectionState === 'connected') {
        const stats = await peerConn.getStats();
        let activeCandidatePair;
        stats.forEach(report => {
                if (report.type === 'transport') {
                activeCandidatePair = stats.get(report.selectedCandidatePairId);
                }
                });
        if (activeCandidatePair) {
            if (timestampPrev === activeCandidatePair.timestamp) {
                return;
            }
            // calculate current bitrate
            const bytesNow = activeCandidatePair.bytesReceived;
            const bitrate = Math.round((bytesNow - bytesPrev) * 8 /
                    (activeCandidatePair.timestamp - timestampPrev));
            bitrateDiv.innerHTML = `<strong>Current Bitrate:</strong> ${bitrate} kbits/sec`;
            timestampPrev = activeCandidatePair.timestamp;
            bytesPrev = bytesNow;
            if (bitrate > bitrateMax) {
                bitrateMax = bitrate;
            }
        }
    }
}

//--------------------//
firebase.initializeApp(config);
init();
